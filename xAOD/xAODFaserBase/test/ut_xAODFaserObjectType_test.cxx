// System include(s):
#include <iostream>

// Local include(s):
#include "xAODFaserBase/FaserObjectType.h"

int main() {

   // Print some random values:
   std::cout << xAOD::FaserType::Track << ", " << xAOD::FaserType::CaloCluster
             << std::endl;
   std::cout << static_cast< xAOD::FaserType::ObjectType >( 1500 ) << std::endl;

   // Return gracefully:
   return 0;
}
