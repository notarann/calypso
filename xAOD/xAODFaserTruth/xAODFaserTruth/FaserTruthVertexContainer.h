// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthVertexContainer.h 622193 2014-10-16 16:08:34Z krasznaa $
#ifndef XAODFASERTRUTH_TRUTHVERTEXCONTAINER_H
#define XAODFASERTRUTH_TRUTHVERTEXCONTAINER_H

// Local include(s):
#include "xAODFaserTruth/FaserTruthVertex.h"
// EDM include(s):
#include "AthContainers/DataVector.h"

namespace xAOD {
   // Alias
   typedef DataVector< FaserTruthVertex > FaserTruthVertexContainer;
}

// Declare a CLID for the class for Athena:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthVertexContainer, 1239726537, 1 )

#endif // XAODFASERTRUTH_TRUTHVERTEXCONTAINER_H
