#include "../LooperKillerTool.h"
#include "../AthenaStackingActionTool.h"
#include "../AthenaTrackingActionTool.h"

DECLARE_COMPONENT( G4UA::LooperKillerTool )
DECLARE_COMPONENT( G4UA::AthenaStackingActionTool )
DECLARE_COMPONENT( G4UA::AthenaTrackingActionTool )
