###############################################################################
# Package: TrackerSimEventTPCnv
################################################################################

# Declare the package name:
atlas_subdir( TrackerSimEventTPCnv )

# External dependencies:
find_package( CLHEP )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_library( TrackerSimEventTPCnv
                   src/TrackerHits/*.cxx
                   PUBLIC_HEADERS TrackerSimEventTPCnv
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                   PRIVATE_DEFINITIONS ${CLHEP_DEFINITIONS}
                   LINK_LIBRARIES GaudiKernel GeneratorObjectsTPCnv TrackerSimEvent AthenaPoolCnvSvcLib StoreGateLib SGtests
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} TestTools Identifier )

atlas_add_dictionary( TrackerSimEventTPCnvDict
                      TrackerSimEventTPCnv/TrackerSimEventTPCnvDict.h
                      TrackerSimEventTPCnv/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} AthenaPoolCnvSvcLib GaudiKernel GeneratorObjectsTPCnv TrackerSimEvent TestTools StoreGateLib SGtests Identifier TrackerSimEventTPCnv AthenaKernel )

