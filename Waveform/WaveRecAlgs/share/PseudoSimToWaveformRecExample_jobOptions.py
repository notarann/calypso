#!/usr/bin/env python

import sys

if __name__ == "__main__":

    fileroot = "SimToRec"
    naive = True

    from AthenaCommon.Logging import log, logging
    from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaCommon.Configurable import Configurable
        
    Configurable.configurableRun3Behavior = True

    log.setLevel(INFO)
    
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-TB00"             # Always needed; must match FaserVersion
    ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
    ConfigFlags.Input.ProjectName = "mc21"                       # Needed to bypass autoconfig
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.GeoModel.FaserVersion     = "FASER-TB00"         # FASER geometry
    ConfigFlags.Common.isOnline = False
    ConfigFlags.GeoModel.Align.Dynamic = False
    
    ConfigFlags.Input.Files = [
        "my.HITS.pool.root"
        #"/bundle/data/FASER/LC_output/BatchOutput/TestBeam/TB.Elec.8.r5.e100.SIM.root"
        ]

    
    ConfigFlags.addFlag("Output.xAODFileName", f"{fileroot}.xAOD.root")
    ConfigFlags.Output.ESDFileName = f"{fileroot}.ESD.root"

    ConfigFlags.lock()

    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg

    acc = MainServicesCfg(ConfigFlags)

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg

    acc.merge(PoolReadCfg(ConfigFlags))
    acc.merge(PoolWriteCfg(ConfigFlags))

    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    itemList = [
        "xAOD::EventInfo#*",
        "xAOD::WaveformHitContainer#*",
        "xAOD::WaveformHitAuxContainer#*",
        ]
    
    acc.merge(OutputStreamCfg(ConfigFlags, "xAOD", itemList, disableEventTag=True))

    from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg
    acc.merge(WaveformReconstructionCfg(ConfigFlags, naive))

    #acc.foreach_component("*").OutputLevel = VERBOSE

    # Execute and finish
    sc = acc.run(maxEvents=1000)

    # Success should be 0
    sys.exit(not sc.isSuccess())
