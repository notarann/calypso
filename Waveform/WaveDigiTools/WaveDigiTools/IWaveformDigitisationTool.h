/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file IWaveformDigitisationTool.h
 * Header file for the IWaveformDigitisationTool class
 * @author Carl Gwilliam, 2021
 */


#ifndef WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H
#define WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H

// Base class
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"

#include "WaveRawEvent/RawWaveformContainer.h"
#include "WaveRawEvent/RawWaveform.h"

#include "Identifier/Identifier.h"

#include "TF1.h"
#include "TRandom3.h"

#include <utility>
#include <map>
#include <vector>


///Interface for waveform digitisation tools
class IWaveformDigitisationTool : virtual public IAlgTool 
{
public:

  // InterfaceID
  DeclareInterfaceID(IWaveformDigitisationTool, 1, 0);

  IWaveformDigitisationTool():
    m_msgSvc         ( "MessageSvc",   "ITrkEventCnvTool" )
  {}

  virtual ~IWaveformDigitisationTool() = default;

  /// Evaluate time kernel over time samples  
  virtual std::vector<float> evaluate_timekernel(TF1* kernel) const = 0;

  /// Generate random baseline 
  virtual float generate_baseline(float mean, float rms) const = 0;
 
  /// Create structure to store pulse for each channel
  template <class T> 
  std::map<Identifier, std::vector<uint16_t>> create_waveform_map(const T* idHelper) const;

  /// Number of time samples
  unsigned int nsamples() const { return m_nsamples; }

private:
  ServiceHandle<IMessageSvc>      m_msgSvc;

protected:
  TRandom3*                       m_random;
  unsigned int                    m_nsamples;
};

#include "WaveDigiTools/IWaveformDigitisationTool.icc"


#endif //WAVEDIGITOOLS_IWAVEFORMDIGITISATIONTOOL_H
