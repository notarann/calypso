#include "CombinatorialKalmanFilterAlg.h"
#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;

using Stepper = Acts::EigenStepper<>;
using Navigator = Acts::Navigator;
using Propagator = Acts::Propagator<Stepper, Navigator>;
using CKF = Acts::CombinatorialKalmanFilter<Propagator, Updater, Smoother>;

using TrackParametersContainer = std::vector<CombinatorialKalmanFilterAlg::TrackParameters>;

struct TrackFinderFunctionImpl
    : public CombinatorialKalmanFilterAlg::TrackFinderFunction {
  CKF trackFinder;

  TrackFinderFunctionImpl(CKF&& f) : trackFinder(std::move(f)) {}

  CombinatorialKalmanFilterAlg::TrackFinderResult operator()(
      const IndexSourceLinkContainer& sourcelinks,
      const TrackParametersContainer& initialParameters,
      const CombinatorialKalmanFilterAlg::TrackFinderOptions& options)
  const override {
    return trackFinder.findTracks(sourcelinks, initialParameters, options);
  };
};

}  // namespace

std::shared_ptr<CombinatorialKalmanFilterAlg::TrackFinderFunction>
CombinatorialKalmanFilterAlg::makeTrackFinderFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry,
    bool resolvePassive, bool resolveMaterial, bool resolveSensitive) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  Stepper stepper(std::move(magneticField));
  Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = resolvePassive;
  cfg.resolveMaterial = resolveMaterial;
  cfg.resolveSensitive = resolveSensitive;
  Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  CKF trackFinder(std::move(propagator));

  // build the track finder functions. owns the track finder object.
  return std::make_shared<TrackFinderFunctionImpl>(std::move(trackFinder));
}
