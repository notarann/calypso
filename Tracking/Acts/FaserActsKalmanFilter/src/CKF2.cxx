#include "CKF2.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "StoreGate/WriteDecorHandle.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointCollection.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrkRIO_OnTrack/RIO_OnTrack.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsRecMultiTrajectory.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"
#include "TrackSelection.h"
#include <algorithm>

#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "Acts/EventData/Measurement.hpp"
#include "Acts/Propagator/PropagatorError.hpp"
#include "Acts/TrackFinding/CombinatorialKalmanFilterError.hpp"

size_t CKF2::TrajectoryInfo::nClusters {0};

using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;
//std::array<Acts::BoundIndices, 2> indices = {Acts::eBoundLoc0, Acts::eBoundLoc1};


CKF2::CKF2(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name, pSvcLocator) {}


StatusCode CKF2::initialize() {
  ATH_CHECK(m_fieldCondObjInputKey.initialize());
  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(m_trackSeedTool.retrieve());
  ATH_CHECK(m_kalmanFitterTool1.retrieve());
  ATH_CHECK(m_createTrkTrackTool.retrieve());
  ATH_CHECK(m_trackCollection.initialize());
  // ATH_CHECK(m_allTrackCollection.initialize());
  ATH_CHECK(m_eventInfoKey.initialize());
  if (m_performanceWriter && !m_noDiagnostics) {
    ATH_CHECK(m_performanceWriterTool.retrieve());
  }
  if (m_statesWriter && !m_noDiagnostics) {
    ATH_CHECK(m_trajectoryStatesWriterTool.retrieve());
  }
  if (m_summaryWriter && !m_noDiagnostics) {
    ATH_CHECK(m_trajectorySummaryWriterTool.retrieve());
  }
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
  m_fit = makeTrackFinderFunction(m_trackingGeometryTool->trackingGeometry(),
                                  m_resolvePassive, m_resolveMaterial, m_resolveSensitive);
  m_kf = makeTrackFitterFunction(m_trackingGeometryTool->trackingGeometry());
  // FIXME fix Acts logging level
  if (m_actsLogging == "VERBOSE") {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
  } else if (m_actsLogging == "DEBUG") {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::DEBUG);
  } else {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::INFO);
  }
  return StatusCode::SUCCESS;
}


StatusCode CKF2::execute() {
  const EventContext& ctx = Gaudi::Hive::currentContext();
  m_numberOfEvents++;

  SG::WriteDecorHandle<xAOD::EventInfo,uint32_t> eventInfo (m_eventInfoKey, ctx);

  SG::WriteHandle trackContainer{m_trackCollection, ctx};
  std::unique_ptr<TrackCollection> outputTracks = std::make_unique<TrackCollection>();

  // SG::WriteHandle allTrackContainer{m_allTrackCollection, ctx};
  // std::unique_ptr<TrackCollection> outputAllTracks = std::make_unique<TrackCollection>();

  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
      = m_trackingGeometryTool->trackingGeometry();

  const FaserActsGeometryContext& faserActsGeometryContext = m_trackingGeometryTool->getGeometryContext();
  auto gctx = faserActsGeometryContext.context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;

  CHECK(m_trackSeedTool->run(m_maskedLayers));
  std::shared_ptr<const Acts::Surface> initialSurface =
      m_trackSeedTool->initialSurface();
  std::shared_ptr<std::vector<Acts::CurvilinearTrackParameters>> initialParameters =
      m_trackSeedTool->initialTrackParameters();
  std::shared_ptr<std::vector<IndexSourceLink>> sourceLinks =
      m_trackSeedTool->sourceLinks();
  double origin = m_trackSeedTool->targetZPosition();

  std::shared_ptr<std::vector<Measurement>> measurements = m_trackSeedTool->measurements();
  std::shared_ptr<std::vector<const Tracker::FaserSCT_Cluster*>> clusters = m_trackSeedTool->clusters();
  std::shared_ptr<std::vector<const Tracker::FaserSCT_SpacePoint*>> spacePoints = m_trackSeedTool->spacePoints();
  std::shared_ptr<std::vector<std::array<std::vector<const Tracker::FaserSCT_Cluster*>, 3>>> seedClusters = m_trackSeedTool->seedClusters();

  TrajectoryInfo::nClusters = sourceLinks->size();
  TrajectoriesContainer trajectories;
  trajectories.reserve(initialParameters->size());

  Acts::PropagatorPlainOptions pOptions;
  pOptions.maxSteps = m_maxSteps;

  Acts::MeasurementSelector::Config measurementSelectorCfg = {
      {Acts::GeometryIdentifier(), {m_chi2Max, m_nMax}},
  };

  Acts::RotationMatrix3 rotation = Acts::RotationMatrix3::Identity();
  rotation.col(0) = Acts::Vector3(0, 0, -1);
  rotation.col(1) = Acts::Vector3(0, 1, 0);
  rotation.col(2) = Acts::Vector3(1, 0, 0);
  Acts::Translation3 trans(0., 0., origin);
  Acts::Transform3 trafo(rotation * trans);
  initialSurface = Acts::Surface::makeShared<Acts::PerigeeSurface>(trafo);

  // Set the CombinatorialKalmanFilter options
  CKF2::TrackFinderOptions options(
      gctx, magFieldContext, calibContext,
      IndexSourceLinkAccessor(), MeasurementCalibrator(*measurements),
      Acts::MeasurementSelector(measurementSelectorCfg),
      Acts::LoggerWrapper{*m_logger}, pOptions, &(*initialSurface));

  // Perform the track finding for all initial parameters
  m_numberOfTrackSeeds += initialParameters->size();
  ATH_MSG_DEBUG("Invoke track finding with " << initialParameters->size() << " seeds.");
  IndexSourceLinkContainer tmp;
  for (const auto& sl : *sourceLinks) {
    tmp.emplace_hint(tmp.end(), sl);
  }

  for (const auto& init : *initialParameters) {
    ATH_MSG_DEBUG("  position: " << init.position(gctx).transpose());
    ATH_MSG_DEBUG("  momentum: " << init.momentum().transpose());
    ATH_MSG_DEBUG("  charge:   " << init.charge());
  }

  auto results = (*m_fit)(tmp, *initialParameters, options);

  // results contain a MultiTrajectory for each track seed with a trajectory of each branch of the CKF.
  // To simplify the ambiguity solving a list of MultiTrajectories is created, each containing only a single track.
  std::list<TrajectoryInfo> allTrajectories;
  for (auto &result : results) {
    if (not result.ok()) {
      // TODO use status bits for different errors
      // result.error() == Acts::CombinatorialKalmanFilterError::NoTrackFound
      if (result.error() == Acts::PropagatorError::StepCountLimitReached ||
          result.error() == Acts::CombinatorialKalmanFilterError::PropagationReachesMaxSteps) 
      {
          try
          {
            if (!eventInfo->updateErrorState(xAOD::EventInfo::SCT, xAOD::EventInfo::Error)) 
            {
              ATH_MSG_WARNING ("Cannot set error state for SCT.");
            }
          }
          catch (...)
          {
              ATH_MSG_DEBUG ("SCT error state is locked.");
          }
      }
      continue;
    }
    CKFResult ckfResult = result.value();
    for (size_t trackTip : ckfResult.lastMeasurementIndices) {
      allTrajectories.emplace_back(TrajectoryInfo(FaserActsRecMultiTrajectory(
          ckfResult.fittedStates, {trackTip}, {{trackTip, ckfResult.fittedParameters.at(trackTip)}})));
    }
  }
  m_numberOfFittedTracks += allTrajectories.size();

  // the list of MultiTrajectories is sorted by the number of measurements using the chi2 value as a tie-breaker
  allTrajectories.sort([](const TrajectoryInfo &left, const TrajectoryInfo &right) {
    if (left.nMeasurements > right.nMeasurements) return true;
    if (left.nMeasurements < right.nMeasurements) return false;
    if (left.chi2 < right.chi2) return true;
    else return false;
  });

  // select all tracks with at least 13 heats and with 6 or less shared hits, starting from the best track
  // TODO use Gaudi parameters for the number of hits and shared hits
  // TODO allow shared hits only in the first station?
  // std::vector<FaserActsRecMultiTrajectory> rawTrajectories {};
  // for (auto raw : allTrajectories)
  // {
  //   rawTrajectories.push_back(raw.trajectory);
  // }
  // for (const FaserActsRecMultiTrajectory &traj : rawTrajectories) {
  //   const auto params = traj.trackParameters(traj.tips().front());
  //   ATH_MSG_DEBUG("Fitted parameters (raw)");
  //   ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
  //   ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
  //   ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
  //   ATH_MSG_DEBUG("  charge:   " << params.charge());
  //   std::unique_ptr<Trk::Track> track = m_createTrkTrackTool->createTrack(gctx, traj);
  //   if (track != nullptr) {
  //     m_numberOfSelectedTracks++;
  //     std::unique_ptr<Trk::Track> track2 = m_kalmanFitterTool1->fit(ctx, gctx, track.get(), Acts::BoundVector::Zero(), m_isMC, origin);
  //     if (track2) {
  //       outputAllTracks->push_back(std::move(track2));
  //     } else {
  //       outputAllTracks->push_back(std::move(track));
  //       ATH_MSG_WARNING("Re-Fit failed.");
  //     }
  //   } else {
  //     ATH_MSG_WARNING("CKF failed.");
  //   }
  // }

  std::vector<FaserActsRecMultiTrajectory> selectedTrajectories {};
  while (not allTrajectories.empty()) {
    TrajectoryInfo selected = allTrajectories.front();
    selectedTrajectories.push_back(selected.trajectory);
    allTrajectories.remove_if([&](const TrajectoryInfo &p) {
      return (p.nMeasurements <= 12) || ((p.clusterSet & selected.clusterSet).count() > 6);
    });
  }

  for (const FaserActsRecMultiTrajectory &traj : selectedTrajectories) {
    const auto params = traj.trackParameters(traj.tips().front());
    ATH_MSG_DEBUG("Fitted parameters");
    ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
    ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
    ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
    ATH_MSG_DEBUG("  charge:   " << params.charge());
    std::unique_ptr<Trk::Track> track = m_createTrkTrackTool->createTrack(gctx, traj);
    if (track != nullptr) {
      m_numberOfSelectedTracks++;
      std::unique_ptr<Trk::Track> track2 = m_kalmanFitterTool1->fit(ctx, gctx, track.get(), Acts::BoundVector::Zero(), m_isMC, origin);
      if (track2) {
        outputTracks->push_back(std::move(track2));
      } else {
        outputTracks->push_back(std::move(track));
        ATH_MSG_WARNING("Re-Fit failed.");
      }
    } else {
      ATH_MSG_WARNING("CKF failed.");
    }
  }

  // run the performance writer
  if (m_statesWriter && !m_noDiagnostics) {
    ATH_CHECK(m_trajectoryStatesWriterTool->write(gctx, selectedTrajectories, m_isMC));
  }
  if (m_summaryWriter && !m_noDiagnostics) {
    ATH_CHECK(m_trajectorySummaryWriterTool->write(gctx, selectedTrajectories, m_isMC));
  }
  if  (m_performanceWriter && !m_noDiagnostics) {
    ATH_CHECK(m_performanceWriterTool->write(gctx, selectedTrajectories));
  }
  // ATH_CHECK(allTrackContainer.record(std::move(outputAllTracks)));
  ATH_CHECK(trackContainer.record(std::move(outputTracks)));

  return StatusCode::SUCCESS;
}


StatusCode CKF2::finalize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::finalize()");
  ATH_MSG_INFO(m_numberOfEvents << " events processed.");
  ATH_MSG_INFO(m_numberOfTrackSeeds << " seeds.");
  ATH_MSG_INFO(m_numberOfFittedTracks << " fitted tracks.");
  ATH_MSG_INFO(m_numberOfSelectedTracks << " selected and re-fitted tracks.");
  return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext CKF2::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}



void CKF2::computeSharedHits(std::vector<IndexSourceLink>* sourceLinks, TrackFinderResult& results) const {
  // Compute shared hits from all the reconstructed tracks
  // Compute nSharedhits and Update ckf results
  // hit index -> list of multi traj indexes [traj, meas]
  static_assert(Acts::SourceLinkConcept<IndexSourceLink>,
                "Source link does not fulfill SourceLinkConcept");

  std::vector<std::size_t> firstTrackOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());
  std::vector<std::size_t> firstStateOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());

  for (unsigned int iresult = 0; iresult < results.size(); iresult++) {
    if (not results.at(iresult).ok()) {
      continue;
    }

    auto& ckfResult = results.at(iresult).value();
    auto& measIndexes = ckfResult.lastMeasurementIndices;

    for (auto measIndex : measIndexes) {
      ckfResult.fittedStates.visitBackwards(measIndex, [&](const auto& state) {
        if (not state.typeFlags().test(Acts::TrackStateFlag::MeasurementFlag))
          return;

        std::size_t hitIndex = state.uncalibrated().index();

        // Check if hit not already used
        if (firstTrackOnTheHit.at(hitIndex) ==
            std::numeric_limits<std::size_t>::max()) {
          firstTrackOnTheHit.at(hitIndex) = iresult;
          firstStateOnTheHit.at(hitIndex) = state.index();
          return;
        }

        // if already used, control if first track state has been marked
        // as shared
        int indexFirstTrack = firstTrackOnTheHit.at(hitIndex);
        int indexFirstState = firstStateOnTheHit.at(hitIndex);
        if (not results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().test(Acts::TrackStateFlag::SharedHitFlag))
          results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);

        // Decorate this track
        results.at(iresult).value().fittedStates.getTrackState(state.index()).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);
      });
    }
  }
}


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;

using Stepper = Acts::EigenStepper<>;
using Navigator = Acts::Navigator;
using Propagator = Acts::Propagator<Stepper, Navigator>;
using CKF = Acts::CombinatorialKalmanFilter<Propagator, Updater, Smoother>;

using TrackParametersContainer = std::vector<CKF2::TrackParameters>;

struct TrackFinderFunctionImpl
    : public CKF2::TrackFinderFunction {
  CKF trackFinder;

  TrackFinderFunctionImpl(CKF&& f) : trackFinder(std::move(f)) {}

  CKF2::TrackFinderResult operator()(
      const IndexSourceLinkContainer& sourcelinks,
      const TrackParametersContainer& initialParameters,
      const CKF2::TrackFinderOptions& options)
  const override {
    return trackFinder.findTracks(sourcelinks, initialParameters, options);
  };
};

}  // namespace

std::shared_ptr<CKF2::TrackFinderFunction>
CKF2::makeTrackFinderFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry,
    bool resolvePassive, bool resolveMaterial, bool resolveSensitive) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  Stepper stepper(std::move(magneticField));
  Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = resolvePassive;
  cfg.resolveMaterial = resolveMaterial;
  cfg.resolveSensitive = resolveSensitive;
  Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  CKF trackFinder(std::move(propagator));

  // build the track finder functions. owns the track finder object.
  return std::make_shared<TrackFinderFunctionImpl>(std::move(trackFinder));
}


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;
using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

struct TrackFitterFunctionImpl
    : public CKF2::TrackFitterFunction {
  Fitter trackFitter;

  TrackFitterFunctionImpl(Fitter &&f) : trackFitter(std::move(f)) {}

  CKF2::KFResult operator()(
      const std::vector<IndexSourceLink> &sourceLinks,
      const Acts::BoundTrackParameters &initialParameters,
      const CKF2::TrackFitterOptions &options)
  const override {
    return trackFitter.fit(sourceLinks, initialParameters, options);
  };
};

}  // namespace


std::shared_ptr<CKF2::TrackFitterFunction>
CKF2::makeTrackFitterFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  auto stepper = Stepper(std::move(magneticField));
  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = false;
  cfg.resolveMaterial = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  Fitter trackFitter(std::move(propagator));
  return std::make_shared<TrackFitterFunctionImpl>(std::move(trackFitter));
}
