#include "CaloRecAlg.h"
#include <math.h>

CaloRecAlg::CaloRecAlg(const std::string& name, 
		       ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator) { 

}

StatusCode CaloRecAlg::initialize() {
  ATH_MSG_INFO(name() << "::initalize()" );

  // Set key to read calo hits from
  ATH_CHECK( m_caloWaveHitContainerKey.initialize() );

  // Set key to read preshower hits from
  ATH_CHECK( m_preshowerWaveHitContainerKey.initialize() );

  // Set key to write container
  ATH_CHECK( m_caloHitContainerKey.initialize() );
  ATH_CHECK( m_preshowerHitContainerKey.initialize() );

  // Initalize tools
  ATH_CHECK( m_recoCalibTool.retrieve() );

  // Store calibrattiion factos in a vector for ease of access
  m_EM_mu_Map[0] = m_calo_ch0_EM_mu;
  m_EM_mu_Map[1] = m_calo_ch1_EM_mu;
  m_EM_mu_Map[2] = m_calo_ch2_EM_mu;
  m_EM_mu_Map[3] = m_calo_ch3_EM_mu;

  return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------
StatusCode CaloRecAlg::finalize() {
  ATH_MSG_INFO(name() << "::finalize()");

  return StatusCode::SUCCESS;
}
//----------------------------------------------------------------------
StatusCode CaloRecAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("Executing");

  ATH_MSG_DEBUG("Run: " << ctx.eventID().run_number() 
		<< " Event: " << ctx.eventID().event_number());

  // Find the input waveform hit containers
  SG::ReadHandle<xAOD::WaveformHitContainer> caloWaveHitHandle(m_caloWaveHitContainerKey, ctx);

  ATH_CHECK( caloWaveHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for WaveformHitContainer " << m_caloWaveHitContainerKey);

  if (caloWaveHitHandle->size() == 0) {
    ATH_MSG_DEBUG("Calorimeter Waveform Hit container found with zero length!");
  }

  SG::ReadHandle<xAOD::WaveformHitContainer> preshowerWaveHitHandle(m_preshowerWaveHitContainerKey, ctx);

  ATH_CHECK( preshowerWaveHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for WaveformHitContainer " << m_preshowerWaveHitContainerKey);

  if (preshowerWaveHitHandle->size() == 0) {
    ATH_MSG_DEBUG("Preshower Waveform Hit container found with zero length!");
  }

  // Find the output waveform container
  SG::WriteHandle<xAOD::CalorimeterHitContainer> caloHitContainerHandle(m_caloHitContainerKey, ctx);
  ATH_CHECK( caloHitContainerHandle.record( std::make_unique<xAOD::CalorimeterHitContainer>(),
					std::make_unique<xAOD::CalorimeterHitAuxContainer>() ) );

  SG::WriteHandle<xAOD::CalorimeterHitContainer> preshowerHitContainerHandle(m_preshowerHitContainerKey, ctx);
  ATH_CHECK( preshowerHitContainerHandle.record( std::make_unique<xAOD::CalorimeterHitContainer>(),
                    std::make_unique<xAOD::CalorimeterHitAuxContainer>() ) );

  ATH_MSG_DEBUG("WaveformsHitContainer '" << caloHitContainerHandle.name() << "' initialized");
  ATH_MSG_DEBUG("WaveformsHitContainer '" << preshowerHitContainerHandle.name() << "' initialized");

  // Loop over calo hits and calibrate each primary hit
  for( const auto& hit : *caloWaveHitHandle ) {
    if (hit->status_bit(xAOD::WaveformStatus::SECONDARY)) continue;

    // Create a new calo hit
    xAOD::CalorimeterHit* calo_hit = new xAOD::CalorimeterHit();
    caloHitContainerHandle->push_back(calo_hit);

    ATH_MSG_DEBUG("calo_hit in channel " << hit->channel() );

    float MIPcharge_ref = m_recoCalibTool->getMIPcharge_ref(hit->channel()); // get reference MIP charge from database

    float charge = hit->integral()/50.0; // divide by 50 ohms to get charge
    ATH_MSG_DEBUG("calo_hit filled has charge of " << charge << " pC");

    float gainRatio = 1.0;
    if (!m_isMC) { // MC already has correct MIP charge stored in MIPcharge_ref, so only need to to HV extrapolation with reral data
      gainRatio = extrapolateHVgain(hit->channel());
    }
    ATH_MSG_DEBUG("HV gain ratio = " << gainRatio );


    float Nmip = (charge * gainRatio) / MIPcharge_ref;
    ATH_MSG_DEBUG("Nmip = " << Nmip );
    calo_hit->set_Nmip(Nmip); // set Nmip value

    float E_dep = Nmip * m_MIP_sim_Edep_calo;
    ATH_MSG_DEBUG("E_dep in MeV = " << E_dep );
    calo_hit->set_E_dep(E_dep);  // set calibrated E_dep value

    float E_EM = Nmip * m_EM_mu_Map[hit->channel()];
    ATH_MSG_DEBUG("Em E in MeV = " << E_EM );
    calo_hit->set_E_EM(E_EM);  // set calibrated E_EM value

    float fit_to_raw_ratio = 1.0;
    if (hit->integral() != 0.0) { // avoid possibility of division by zero error
      fit_to_raw_ratio = hit->raw_integral() / hit->integral();
    }
    calo_hit->set_fit_to_raw_ratio(fit_to_raw_ratio); // set fit-to-raw-ratio that can be used to take any of the calibrated values to what they would be if we used the raw integral instead of the fit integral

    calo_hit->set_channel(hit->channel());  // set channel number

    calo_hit->clearWaveformLinks();
    calo_hit->addHit(caloWaveHitHandle.get(), hit);  // create link to calo waveform hit
  }

  ATH_MSG_DEBUG("CaloHitContainer '" << caloHitContainerHandle.name() << "' filled with "<< caloHitContainerHandle->size() <<" items");

  for( const auto& hit : *preshowerWaveHitHandle ) {
    if (hit->status_bit(xAOD::WaveformStatus::SECONDARY)) continue;

    // Create a new preshower hit
    xAOD::CalorimeterHit* preshower_hit = new xAOD::CalorimeterHit();
    preshowerHitContainerHandle->push_back(preshower_hit);

    ATH_MSG_DEBUG("preshower_hit in channel " << hit->channel() );

    float MIPcharge_ref = m_recoCalibTool->getMIPcharge_ref(hit->channel()); // get reference MIP charge from database

    float charge = hit->integral()/50.0; // divide by 50 ohms to get charge
    ATH_MSG_DEBUG("preshower_hit filled has charge of " << charge << " pC");

    float Nmip = charge / MIPcharge_ref;
    ATH_MSG_DEBUG("Nmip = " << Nmip );
    preshower_hit->set_Nmip(Nmip); // set Nmip value

    float E_dep = Nmip * m_MIP_sim_Edep_preshower;
    ATH_MSG_DEBUG("E_dep in GeV = " << E_dep );
    preshower_hit->set_E_dep(E_dep);  // set calibrated E_dep value

    float fit_to_raw_ratio = 1.0;
    if (hit->integral() != 0.0) { // avoid possibility of division by zero error
      fit_to_raw_ratio = hit->raw_integral() / hit->integral();
    }
    preshower_hit->set_fit_to_raw_ratio(fit_to_raw_ratio); // set fit-to-raw-ratio that can be used to take any of the calibrated values to what they would be if we used the raw integral instead of the fit integral
    
    preshower_hit->set_channel(hit->channel());  // set channel number

    preshower_hit->clearWaveformLinks();
    preshower_hit->addHit(preshowerWaveHitHandle.get(), hit);  // create link to preshower waveform hit
  }

  ATH_MSG_DEBUG("PreshowerHitContainer '" << preshowerHitContainerHandle.name() << "' filled with "<< preshowerHitContainerHandle->size() <<" items");


  return StatusCode::SUCCESS;
}

//----------------------------------------------------------------------
float CaloRecAlg::extrapolateHVgain(int channel) const {
  float PMT_hv = m_recoCalibTool->getHV(channel);
  float PMT_hv_ref = m_recoCalibTool->getHV_ref(channel);
  TF1 gaincurve = m_recoCalibTool->get_PMT_HV_curve(channel);

  float gaincurve_atHV = gaincurve.Eval(PMT_hv);
  float gaincurve_atHVref = gaincurve.Eval(PMT_hv_ref);

  return ( gaincurve_atHVref / gaincurve_atHV ) * pow( PMT_hv_ref / PMT_hv , 6.6);
}
//----------------------------------------------------------------------

